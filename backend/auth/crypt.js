/**
 * January 18, 2016, Rahil Shaikh
 * https://ciphertrick.com/salt-hash-passwords-using-nodejs-crypto/
 */

const crypto = require("crypto")
const {
    saltyNumber,
    codeyNumber,
    secretKey,
    secretIv,
    secretAlg,
} = require("../literals/crypt_literals")

//HASH

/**
 * generates random string of characters i.e salt
 * @function
 * @param {number} length - Length of the random string.
 */
function genRndStr(length) {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex') /** convert to hexadecimal format */
        .slice(0, length)   /** return required number of characters */
}
/**
 * hash password with sha512.
 * @function
 * @param {string} password - List of required fields.
 * @param {string} salt - Data to be validated.
 */
function sha512(password, salt) {
    var hash = crypto.createHmac('sha512', salt) /** Hashing algorithm sha512 */
    hash.update(password)
    var passwordHash = hash.digest('hex')
    return passwordHash
}
function genRndCode() {
    return genRndStr(codeyNumber)
}
function genRndSalt() {
    return genRndStr(saltyNumber)
}
function saltHashPassword(passwd) {
    const salt = genRndSalt()
    const passwdData = sha512(passwd, salt)
    return {
        salt: salt,
        password: passwdData
    }
}

function checkHashPassword(passwdHashed, passwd, salt) {
    return passwdHashed === sha512(passwd, salt)
}

function hashPassword(password, salt) {
    return sha512(password, salt);
}

//ENCRYPT

function genFullCode(emailphone, code) {
    return emailphone + "@" + code
}
function getEPCode(fullCode) {
    const n = fullCode.lastIndexOf("@");
    return {
        email: fullCode.slice(0, n),
        code: fullCode.slice(n + 1),
    }
}

/**
 * Last Updated: Juli 30, 2020
 * https://attacomsian.com/blog/nodejs-encrypt-decrypt-data
 */

function encrypt(text) {
    const cipher = crypto.createCipheriv(secretAlg, secretKey, secretIv)
    const encrypted = Buffer.concat([cipher.update(text), cipher.final()])
    return encrypted.toString('hex')
}

function decrypt(hash) {
    const decipher = crypto.createDecipheriv(secretAlg, secretKey, secretIv)
    const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash, 'hex')), decipher.final()])
    return decrpyted.toString()
}

module.exports = {
    saltHashPassword,
    checkHashPassword,
    hashPassword,
    genRndCode,
    encrypt,
    decrypt,
    genFullCode,
    getEPCode,
}