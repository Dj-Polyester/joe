//tests for alphabetic
const isValid = (val) => /^[A-Za-z ]+$/.test(val)

module.exports = {
    isValid: isValid,
}