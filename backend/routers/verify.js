const express = require("express")
const router = new express.Router()
const pool = require("../connect/postgres")
const format = require("pg-format")
const crypt = require("../auth/crypt")
const sendEmail = require("../emails/verification")
const sendSMS = require("../SMS/verification")
const {
    insertPendingApprovals,
    findPendingApprovals,
    updatePendingApprovals,
    updatePendingApprovalsCode,
} = require("../literals/signup_query_literals")

router.post("/verify/init", async (req, res) => {
    let client
    try {
        client = await pool.connect()
        const { name, email, phoneNumber } = await req.body

        let emailphone, resSuccess, EPfunc
        if (email) {
            emailphone = email
            resSuccess = { code: 1001, message: "Succesfully sent mail" }
            EPfunc = sendEmail
        }
        else if (phoneNumber) {
            emailphone = phoneNumber
            resSuccess = { code: 1002, message: "Succesfully sent SMS" }
            EPfunc = sendSMS
        }
        else {
            return res.send({ code: 2004, message: "Email and phone number is null" })
        }

        const findpendingapprovals = format(findPendingApprovals, emailphone)
        const findResult = await client.query(findpendingapprovals)

        if (findResult.rowCount) {
            return res.send({ code: 2005, message: "Already an existing record" })
        }
        else {
            const codePlain = crypt.genRndCode()
            const { password: code, salt } = crypt.saltHashPassword(codePlain)

            const insertpendingapprovals = format(insertPendingApprovals, email, code, false, salt)
            await client.query(insertpendingapprovals)

            await EPfunc(emailphone, name, codePlain)
            return res.send(resSuccess)
        }
    } catch (error) {
        res.status(500).send({ code: 2006, message: "Error sending email/SMS" })
        console.log(error);
        await pool.end()
    }
    finally {
        await client.release()
    }
})

router.post("/verify/update", async (req, res) => {
    let client
    console.log("/verify/update")
    console.log(req.body)
    try {
        client = await pool.connect()
        const { name, email, phoneNumber } = await req.body

        let emailphone, resSuccess, EPfunc
        if (email) {
            emailphone = email
            resSuccess = { code: 1003, message: "Succesfully sent mail" }
            EPfunc = sendEmail
        }
        else if (phoneNumber) {
            emailphone = phoneNumber
            resSuccess = { code: 1004, message: "Succesfully sent SMS" }
            EPfunc = sendSMS
        }
        else {
            return res.status(400).send({ code: 2007, message: "Email and phone number is null" })
        }

        const findpendingapprovals = format(findPendingApprovals, emailphone)
        const findResult = await client.query(findpendingapprovals)

        if (findResult.rowCount === 1) {
            const codePlain = crypt.genRndCode()
            const { password: code, salt } = crypt.saltHashPassword(codePlain)

            const updatependingapprovals = format(updatePendingApprovalsCode, code, salt, email)
            await client.query(updatependingapprovals)

            await EPfunc(resSuccess, name, codePlain)
            return res.send(resSuccess)
        }
        else {
            return res.status(500).send({ code: 2008, message: "Multiple or none" })
        }
    } catch (error) {
        res.status(500).send({ code: 2009, message: "Error sending email/SMS" })
        console.log(error);
        await pool.end()
    }
    finally {
        await client.release()
    }
})

router.post("/verify/check", async (req, res) => {
    let client
    console.log(req.body)
    try {
        client = await pool.connect()

        const { emailphone, code } = req.body

        const findpendingapprovals = format(findPendingApprovals, emailphone)
        const findResult = await client.query(findpendingapprovals)
        if (findResult.rowCount == 1) {
            const _2bapproved = findResult.rows[0]
            if (crypt.checkHashPassword(_2bapproved.code, code, _2bapproved.salt)) {
                const updatependingapprovals = format(updatePendingApprovals, true, emailphone)
                await client.query(updatependingapprovals)
                return res.send({ code: 1005, message: "Account is confirmed" })
            } else {
                return res.send({ code: 2010, message: "Account cannot be confirmed" })
            }
        }
        else {
            return res.send({ code: 2011, message: "Multiple or none" })
        }
    } catch (error) {
        console.log(error)
        res.status(500).send({ code: 2012, message: "Error checking email/SMS" })
        await pool.end()
    }
    finally {
        await client.release()
    }
})

module.exports = router
