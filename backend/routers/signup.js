const express = require("express")
const router = new express.Router()
const client = require("../connect/mongo")
const pool = require("../connect/postgres")
const format = require("pg-format")
const crypt = require("../auth/crypt")
const {
    findUniqueUsername,
    findUniqueEmailPhone,
} = require("../literals/signup_query_literals")

router.post("/signup", async (req, res) => {
    console.log(req.body)
    try {
        //const { name, username, email, phoneNumber, plainPasswd } = req.body

        const db = await client.connect()
        const users = await db.collection("users")
        const user = { ...req.body, ...crypt.saltHashPassword(req.body.plainPasswd) }
        delete user.plainPasswd

        await users.insertOne(user)

    } catch (error) {
        await client.close()
        res.status(500).send()
        console.log(error)
    }
})

//check for unique credentials
router.post("/signup/unique", async (req, res) => {
    let client
    console.log(req.body)
    try {
        const { username, email, phoneNumber } = req.body

        client = await pool.connect()
        let taken = false
        let responseStr = ""


        if (username) {
            const finduniqueemailphone = format(findUniqueUsername, username)
            const findResult = await client.query(finduniqueemailphone)
            if (findResult.rowCount > 0) {
                responseStr += "user name, "
                taken = true
            }
        }
        else {
            return res.status(400).send({ code: 2002, message: "Username is null" })
        }
        if (email) {
            const finduniqueemailphone = format(findUniqueEmailPhone, email)
            const findResult = await client.query(finduniqueemailphone)
            if (findResult.rowCount > 0) {
                responseStr += "email, "
                taken = true
            }
        }
        else if (phoneNumber) {
            const finduniqueemailphone = format(findUniqueEmailPhone, phoneNumber)
            const findResult = await client.query(finduniqueemailphone)
            if (findResult.rowCount > 0) {
                responseStr += "phone number, "
                taken = true
            }
        }
        else {
            return res.status(400).send({ code: 2003, message: "Email and phone number is null" })
        }

        responseStr = responseStr.slice(0, -2)

        const responseObj =
            taken ? { code: 2000, message: responseStr } : { code: 1000, message: "none" }

        return res.send(responseObj)
    } catch (error) {
        console.log(error)
        res.status(500).send({ code: 2001, message: "Error checking unique credentials." })
        await pool.end()
    }
    finally {
        await client.release()
    }
})

module.exports = router