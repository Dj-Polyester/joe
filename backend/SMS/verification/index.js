const Nexmo = require('nexmo')
const { NEXMO_API_KEY, NEXMO_API_SECRET, FROM } = require("../../literals/sms_literals")

const nexmo = new Nexmo({
    apiKey: NEXMO_API_KEY,
    apiSecret: NEXMO_API_SECRET
})
//+90544483866

module.exports = async (to, name, code) => {
    console.log(`sending SMS to ${to} with name ${name}`)
    const text = `
Dear ${name},\n
please click the link below to verify your account. After clicking, please make sure your registration proceeds. Otherwise, please contact us.\n
http://192.168.1.28:3000/verify/${code}
`
    nexmo.message.sendSms(FROM, to, text, { type: 'unicode' }, (err, responseData) => {
        if (err) {
            console.log(err);
        } else {
            if (responseData.messages[0]['status'] === "0") {
                console.log("SMS sent successfully.");
            } else {
                console.log(`SMS failed with error: ${responseData.messages[0]['error-text']}`);
            }
        }
    })
}
