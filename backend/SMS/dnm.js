const util = require('util');
const Nexmo = require('nexmo');

const { NEXMO_API_KEY, NEXMO_API_SECRET, from } = require("../literals/sms_literals")

const nexmo = new Nexmo({
  apiKey: NEXMO_API_KEY,
  apiSecret: NEXMO_API_SECRET
}, { debug: true });


const sendSms = util.promisify(nexmo.message.sendSms);

; (async function () {
  const { err, res } = await sendSms(from, "544483866", "...", { type: 'unicode' });
  if (err) { console.log(err) } else {
    if (res.messages[0]['status'] === "0") {
      console.log("Successfully sent SMS.");
    } else {
      console.log(`SMS failed with error: ${res.messages[0]['error-text']}`);
    }
  }
})()

