const Email = require("email-templates")
const nodemailer = require("nodemailer")
const path = require("path")
const configinfo = require("./configinfo")
const { senderName, senderMail } = require("./senderinfo")

const transporter = nodemailer.createTransport(configinfo)

const email = new Email({
    message: {
        from: `"${senderName}" <${senderMail}>`
    },
    send: false,
    transport: transporter,
    preview: true,
    juiceResources: {
        preserveImportant: true,
        webResources: {
            relativeTo: path.join(__dirname, "static"),
            images: true
        }
    },
    views: {
        root: __dirname
    }
});

module.exports = async (userEmail, name, code) => {
    try {
        console.log(`sending mail to ${userEmail} with name ${name}`)

        await email
            .send({
                template: 'templates',
                message: {
                    to: `"${name}" <${userEmail}>`
                },
                locals: {
                    name: name,
                    code: code
                }
            })
        console.log("Succesfully sent email")
    } catch (error) {
        console.log(error)
    }
}