//this file is not used because some mail providers do not have dynamic functionality
const lottieAnimContainer = document.getElementById("lottie-anim")
const confirmButt = document.getElementById("confirm-butt")
const confirmTxt = document.getElementById("confirm-txt")
const explanationTxt = document.getElementById("explanation-txt")
const codeElem = document.getElementById("code-txt")

const lottieAnim = " \
<lottie-player \
  src=\"https://assets8.lottiefiles.com/packages/lf20_oUXj84.json\" \
  background=\"transparent\" \
  speed=\"2.5\" \
  style=\"width: 32px; height: 32px\" \
  autoplay \
></lottie-player> \
"
"#lottie-anim script(src='https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js')"

confirmButt.addEventListener("click", async () => {

    try {
        const code = codeElem.innerText
        console.log(codeElem)
        console.log(code)
        confirmButt.disabled = true
        const response = await fetch(`http://192.168.1.28:3000/signup/verify/${code}`)
        console.log(response)
        const data = await response.json()
        console.log(data)
        // console.log(data)
        let answer = null
        if (data.code === 1002) {
            confirmTxt.innerText = "Confirmed"
            lottieAnimContainer.innerHTML = lottieAnim
        }
        else if (data.code === 2003) {
            answer = "We are sorry for the inconvinience. Please try again later."
        }
        else {
            answer = "Your account cannot be confirmed. Please try again. If the problem persists, contact us using the link below."
        }
        if (answer) {
            explanationTxt.innerText = answer
        }
    }
    catch (error) {
        console.log(error)
    }

})
/*, {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(code) // body data type must match "Content-Type" header
            } */
