/**
 * @copyright
 * https://www.w3schools.com/howto/howto_js_copy_clipboard.asp
 */

const cpyButt = document.getElementById("copy-txt-btn")
const cpyHint = document.getElementById("copy-txt-hint")

cpyButt.addEventListener("click", () => {
    var copyText = document.getElementById("code-txt");
    var textArea = document.createElement("textarea");
    textArea.value = copyText.textContent;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
    cpyHint.innerText = "copied"
})
cpyButt.addEventListener("mouseleave", () => {
    cpyHint.innerText = "copy"
})
