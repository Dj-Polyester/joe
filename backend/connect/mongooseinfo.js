const username = "polyester"
const password = "08141229Bk"
const host = "localhost"
const port = 27017
const dbname = "profind"
const mongooseurl = `mongodb://${username}:${password}@${host}:${port}/${dbname}?authSource=admin`

module.exports = {
    mongooseurl: mongooseurl,
    port: port,
    host: host,
}