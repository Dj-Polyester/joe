const { Pool } = require('pg');
const fs = require("fs")
const { promisify } = require("util")
const readFile = promisify(fs.readFile)
const path = require("path")

let pool = null
async function connect() {
    if (!pool) {
        try {
            const data = await readFile(path.join(__dirname, "postgresinfo.json"))
            const postgresinfo = await JSON.parse(data)
            pool = new Pool(postgresinfo)
            console.log(`Opened a postgres connection pool on host ${postgresinfo.host} port ${postgresinfo.port}`)
        }
        catch (error) {
            if (pool) pool.end()
            return console.log(error)
        }
    }
    return await pool.connect()
}
async function end() {
    const close = await pool.end()
    pool = null
    return close
}
module.exports = {
    connect: connect,
    end: end,
}
