const mongoose = require("mongoose")
const { mongooseurl: url, port, host } = require("./mongooseinfo")


mongoose.Promise = global.Promise


    ; (async () => {
        try {
            await mongoose.connect(url, {
                useNewUrlParser: true,
                useCreateIndex: true,
                useUnifiedTopology: true,
                useFindAndModify: false,
            })
        } catch (error) {
            console.log(error)
            return mongoose.connection.close()
        }
        console.log(`Connected to mongo server on host ${host} port ${port}`)
    })()