const { MongoClient } = require("mongodb")
const fs = require("fs")
const { promisify } = require("util")
const readFile = promisify(fs.readFile)
const path = require("path")

let client = null
let db = null
async function connect() {
    if (!client) {
        try {
            const data = await readFile(path.join(__dirname, "mongoinfo.json"))
            const { username, password, port, host, dbname } = await JSON.parse(data)
            const url = `mongodb://${username}:${password}@${host}:${port}/?authSource=admin`

            client = new MongoClient(url, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });
            await client.connect()

            console.log(`Connected to mongo server on host ${host} port ${port}`)
            db = client.db(dbname)
            console.log(`Connected to database ${dbname}`)
            return db
        } catch (error) {
            if (client)
                await client.close()
            return console.log(error)
        }
    }
    else
        return db
}

async function close() {
    const close = await client.close()
    client = null
    return close
}

module.exports = {
    connect: connect,
    close: close,
}
