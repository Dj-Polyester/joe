const pool = require("../connect/postgres")


    ; (async () => {
        let client
        try {
            client = await pool.connect()
            console.log(await require("./dwa2skills")(client, "Review art or design materials."))
        } catch (error) {
            console.log(error)
        }
        finally {
            client.release()
            pool.end()
        }
    })()