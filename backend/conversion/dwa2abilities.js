const format = require("pg-format")
const pool = require("../connect/postgres")
const {
    dwa2abilitiesLiteral,
} = require("../literals/onet_query_literals")

module.exports = async (client, title, dwa) => {
    try {
        const dwa2abilitiesliteral = format(dwa2abilitiesLiteral, dwa, title)
        const result = await client.query(dwa2abilitiesliteral)
        return result.rows
    } catch (error) {
        return console.log(error)
    }
}