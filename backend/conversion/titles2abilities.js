const format = require("pg-format")
const pool = require("../connect/postgres")
const {
    titles2abilitiesLiteral,
} = require("../literals/onet_query_literals")

module.exports = async (client, title) => {
    try {
        const titles2abilitiesliteral = format(titles2abilitiesLiteral, title)
        const result = await client.query(titles2abilitiesliteral)
        return result.rows
    } catch (error) {
        return console.log(error)
    }
}