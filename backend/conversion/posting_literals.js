const createTmpTasksLiteral = " \
CREATE TEMPORARY TABLE temp_tasks AS \
SELECT task_id \
FROM _tasks \
WHERE onetsoc_code IN \
( \
SELECT onetsoc_code \
FROM _alternate_titles \
WHERE alternate_title = %L \
); \
"
const getTmpTasksLiteral = "SELECT * FROM temp_tasks;"
const createTmpWaLiteral = "CREATE TEMPORARY TABLE temp_wa AS \
SELECT wa_id \
FROM _dwa2wa \
WHERE dwa_id IN ( \
    SELECT dwa_id \
    FROM _tasks2dwa \
    WHERE task_id IN (select task_id from temp_tasks) \
    GROUP BY dwa_id \
) \
GROUP BY wa_id \
"
const getSkillsLiteral = " \
SELECT skills_element_id \
FROM _skills2wa \
WHERE work_activities_element_id IN (SELECT * FROM temp_wa) \
GROUP BY skills_element_id; \
"
const getAbilitiesLiteral = " \
SELECT abilities_element_id \
FROM _abilities2wa \
WHERE work_activities_element_id IN (SELECT * FROM temp_wa) \
GROUP BY abilities_element_id; \
"

module.exports = {
    createTmpTasksLiteral: createTmpTasksLiteral,
    getTmpTasksLiteral: getTmpTasksLiteral,
    createTmpWaLiteral: createTmpWaLiteral,
    getSkillsLiteral: getSkillsLiteral,
    getAbilitiesLiteral: getAbilitiesLiteral,
}