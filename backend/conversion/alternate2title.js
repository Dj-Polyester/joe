const format = require("pg-format")
const pool = require("../connect/postgres")
const {
    createTitlesLiteral,
} = require("../literals/onet_query_literals")

module.exports = async (client, alternate_title) => {
    try {
        alternate_title = "%" + alternate_title + "%"
        const createtitlesliteral = format(createTitlesLiteral, alternate_title)
        const result = await client.query(createtitlesliteral)
        return result.rows
    } catch (error) {
        return console.log(error)
    }
}