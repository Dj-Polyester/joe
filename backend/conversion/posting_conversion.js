const format = require("pg-format")
const pool = require("../connect/postgres")
const { host, port } = require("../connect/postgresinfo")
const {
    createTmpTasksLiteral,
    getTmpTasksLiteral,
    createTmpWaLiteral,
    getSkillsLiteral,
    getAbilitiesLiteral,
} = require("./posting_literals")
const { isValid } = require("./validate")
var client;

const regConversion = async (alternate_title) => {
    try {
        client = await pool.connect()
        console.log(`Connected to postgres server on host ${host} port ${port}`)

        if (isValid(alternate_title)) {

            const queryCreateTmpTasks = format(
                createTmpTasksLiteral,
                alternate_title,
            )
            await client.query(queryCreateTmpTasks)
            const tasks = await client.query(getTmpTasksLiteral)
            //console.log(tasks.rows)
            await client.query(createTmpWaLiteral)
            const skills = await client.query(getSkillsLiteral)
            console.log(skills.rows)
            //console.log(skills.rows.length)
            const abilities = await client.query(getAbilitiesLiteral)
            console.log(abilities.rows)
            //console.log(abilities.rows.length)

        }
        else {
            throw Error("Could not find the requested item.")
        }

    } catch (error) {
        console.log(error);
        pool.end()
    }
    finally {
        client.release()
    }
    console.log("Success: Title obtained.");
}