const format = require("pg-format")
const pool = require("../connect/postgres")
const {
    titles2skillsLiteral,
} = require("../literals/onet_query_literals")
/**
 * takes a dwa, gives the most dominant title with factors for pts to be added to skills
 * 
 * @param {string} client - postgres client
 * @param {string} title 
 */
module.exports = async (client, title) => {
    try {
        const titles2skillsliteral = format(titles2skillsLiteral, title)
        const result = await client.query(titles2skillsliteral)
        return result.rows
    } catch (error) {
        return console.log(error)
    }
}