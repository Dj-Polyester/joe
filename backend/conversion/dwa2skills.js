const format = require("pg-format")
const pool = require("../connect/postgres")
const {
    dwa2skillsLiteral,
} = require("../literals/onet_query_literals")
/**
 * takes a dwa, gives the most dominant title with factors for pts to be added to skills
 * 
 * @param {string} client - postgres client
 * @param {string} dwa 
 */
module.exports = async (client, dwa) => {
    try {
        const dwa2skillsliteral = format(dwa2skillsLiteral, dwa)
        const result = await client.query(dwa2skillsliteral)
        return result.rows
    } catch (error) {
        return console.log(error)
    }
}