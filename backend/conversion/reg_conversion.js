const format = require("pg-format")
const pool = require("../connect/postgres")
const {
    createTitlesLiteral,
    getSkillsLiteral,
    getAbilitiesLiteral,
    getTechSkillsLiteral,
    getJobZonesLiteral,
} = require("../literals/reg_literals")
const { isValid } = require("./validate")
var client;

const regConversion = async (alternate_title) => {
    try {
        client = await pool.connect()
        console.log(`Connected to postgres server on host ${host} port ${port}`)

        if (isValid(alternate_title)) {
            const queryCreateTmpTasks = format(
                createTitlesLiteral,
                alternate_title,
            )
            await client.query(queryCreateTmpTasks)
            const skills = await client.query(getSkillsLiteral)
            const abilities = await client.query(getAbilitiesLiteral)
            const techSkills = await client.query(getTechSkillsLiteral)
            const jobZones = await client.query(getJobZonesLiteral)
        }
        else {
            throw Error("Could not find the requested item.")
        }

    } catch (error) {
        console.log(error);
        pool.end()
    }
    finally {
        client.release()
    }
    console.log("Success: Title obtained.");
}
