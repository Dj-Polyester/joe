package com.example.profind

import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import kotlinx.android.synthetic.main.activity_verification.*
import kotlinx.android.synthetic.main.activity_verification.instructionTxt
import kotlinx.android.synthetic.main.activity_verification.noticeTxt
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.json.JSONObject


class VerificationActivity : BaseActivity() {
    val CODE_TIME: Long = 5//in min
    val countdowntimer: CountDownTimer by lazy {
        object : CountDownTimer(CODE_TIME*60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val color =
                    if (millisUntilFinished > 10000)
                        Color.parseColor("#000000")
                    else
                        ContextCompat.getColor(this@VerificationActivity, R.color.noticeColor)
                val time: Map<String, Int> = calcTime(millisUntilFinished / 1000)

                //Log.i("clrBoizz",color.toString())
                signupCode.setTextColor(color)
                signupCode.text = String.format("%02d:%02d",time["mins"],time["secs"])
            }
            override fun onFinish() {

                signupCode.setTextColor(Color.parseColor("#000000"))
                signupCode.text = "00:00"

                codeBtn.text = resources.getString(R.string.resend)
                setOnRetryPress(codeBtn)

                noticeTxt.text = resources.getString(R.string.codeResendTxt)
                instructionTxt.text = ""
            }
        }
    }

    var initFlag: Boolean = true
    val username: String? by lazy { "yoman" }
    val name: String? by lazy { "boman asdfg" }
    val emailPhoneNumberStr: String? by lazy { "email" }
    val emailPhoneNumberTxt: String? by lazy { "anonymous_571@outlook.com" }

    val instructionCodeTxt: String by lazy {
        val emailSMS: String = if (emailPhoneNumberStr == "email") "email" else "SMS"
        "Please verify your account by the link that is sent to $emailPhoneNumberTxt via $emailSMS"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification)
        setOnVerifyPress(codeBtn)

        /*val name: String? = intent.getStringExtra("name")
        val username: String? = intent.getStringExtra("username")
        val emailPhoneNumberTxt: String? = intent.getStringExtra("emailPhoneNumberTxt")
        val emailPhoneNumberStr: String? = intent.getStringExtra("emailPhoneNumberStr")
        val password = intent.getStringExtra("password")*/

        resetVerification()
    }


    fun sendCheckCode(code: String) {
        val url = "http://${serverinfo["host"]}:${serverinfo["port"]}/verify/check"
        val jsonObj = JSONObject("{code:\"$code\", \"emailphone\":\"$emailPhoneNumberTxt\"}")

        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, url, jsonObj,
            { response ->
                val code = response["code"]
                if (code.equals(1005)) {
                    noticeTxt.text = "Your account is confirmed."
                }
                else if (code.equals(2010)) {
                    noticeTxt.text = "Invalid code."
                }
                else {
                    noticeTxt.text = "We are sorry for the inconvinience. Please try again later."
                }
                Log.i("verifyCheckRequest",response.toString())
            },
            { error ->
                Log.i("verifyCheckRequest",error.toString())
            }
        )

        AppVolleyRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)

    }

    fun sendSetCode() {
        lateinit var suffixPath: String
        if (initFlag) {
            suffixPath = "init"
            initFlag = false
        }
        else suffixPath = "update"

        val url = "http://${serverinfo["host"]}:${serverinfo["port"]}/verify/$suffixPath"
        val jsonObj = JSONObject("{name:\"$name\", \"$emailPhoneNumberStr\":\"$emailPhoneNumberTxt\"}")
        Log.i("sendSetCode",initFlag.toString())
        Log.i("sendSetCode",url.toString())
        Log.i("sendSetCode",jsonObj.toString())

        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, url, jsonObj,
            { response ->
                Log.i("sendSetCode",response.toString())
            },
            { error ->
                Log.i("sendSetCode",error.toString())
            }
        )

        AppVolleyRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }

    fun resetVerification() {
        sendSetCode()

        noticeTxt.text = ""
        codeBtn.text = resources.getString(R.string.submit)
        setOnVerifyPress(codeBtn)
        instructionTxt.text = instructionCodeTxt
        countdowntimer.start()
    }

    fun setOnVerifyPress(btn: TextView) {
        btn.setOnClickListener {
            onVerifyPress()
        }
    }
    fun setOnRetryPress(btn: TextView) {
            btn.setOnClickListener {
                onRetryPress()
            }
        }

    fun calcTime(secs: Long): Map<String, Int> {
        return mapOf<String, Int>("mins" to (secs / 60).toInt(), "secs" to (secs % 60).toInt())
    }

    fun onVerifyPress() {
        Log.i("pressed","onVerifyPress")
        sendCheckCode(codeInp.text.toString())
    }
    fun onRetryPress() {
        Log.i("pressed","onRetryPress")
        resetVerification()
    }

}