//
// Copyright © 2018 Kalpesh Talkar. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// For support: https://gist.github.com/KalpeshTalkar/e6a50efd08a3d5d142e47da559936bb7
//
package com.example.profind

import android.util.Patterns
import android.widget.EditText
import java.util.regex.Pattern

/**
 * Created by Kalpesh on 23/01/18.
 *
 * This class is used to validate data like email, phone, password policy, etc.
 * It also sets error to the EditText or TextInputLayout of the EditText if needed.
 */
class Validator {
    companion object {
        /**
         * Checks if the name is valid.
         * @param data - can be EditText or String
         * @param updateUI - if true and if data is EditText, the function sets error to the EditText or its TextInputLayout
         * @return - true if the name is valid.
         */
        fun isValidName(str: String, updateUI: Boolean = true): Boolean {
            val valid = str.trim().length > 2

            return valid
        }

        /**
         * Checks if the email is valid.
         * @param data - can be EditText or String
         * @return - true if the email is valid.
         */
        fun isValidEmail(str: String): Boolean {
            val valid = Patterns.EMAIL_ADDRESS.matcher(str).matches()

            return valid
        }

        /**
         * Checks if the phone is valid.
         * @param data - can be EditText or String
         * @return - true if the phone is valid.
         */
        fun isValidPhone(str: String): Boolean {
            val valid = Patterns.PHONE.matcher(str).matches()

            return valid
        }

        /**
         * Checks if the password is valid as per the following password policy.
         * Password should be minimum 8 characters long.
         * Password should contain at least one number.
         * Password should contain at least one capital letter.
         * Password should contain at least one small letter.
         * Password should contain at least one special character.
         * Allowed special characters: "~!@#$%^&*()-_=+|/,."';:{}[]<>?"
         *
         * @param data - can be EditText or String
         * @return - true if the password is valid as per the password policy.
         */
        fun isValidPassword(str: String): Boolean {
            var valid = true

            // Password policy check
            // Password should be minimum minimum 8 characters long
            if (str.length < 8) {
                valid = false
            }
            // Password should contain at least one number
            var exp = ".*[0-9].*"
            var pattern = Pattern.compile(exp, Pattern.CASE_INSENSITIVE)
            var matcher = pattern.matcher(str)
            if (!matcher.matches()) {
                valid = false
            }

            // Password should contain at least one capital letter
            exp = ".*[A-Z].*"
            pattern = Pattern.compile(exp)
            matcher = pattern.matcher(str)
            if (!matcher.matches()) {
                valid = false
            }

            // Password should contain at least one small letter
            exp = ".*[a-z].*"
            pattern = Pattern.compile(exp)
            matcher = pattern.matcher(str)
            if (!matcher.matches()) {
                valid = false
            }

            // Password should contain at least one special character
            // Allowed special characters : "~!@#$%^&*()-_=+|/,."';:{}[]<>?"
            exp = ".*[~!@#\$%\\^&*()\\-_=+\\|\\[{\\]};:'\",<.>/?].*"
            pattern = Pattern.compile(exp)
            matcher = pattern.matcher(str)
            if (!matcher.matches()) {
                valid = false
            }

            return valid
        }
    }

}