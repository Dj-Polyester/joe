package com.example.profind

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import kotlinx.android.synthetic.main.activity_signup_unique.*
import org.json.JSONObject


class SignupUniqueActivity : BaseActivity() {
    var switchEmailPhoneNumber: Boolean = true //if true email, else phoneNumber
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup_unique)

        onFocusSignup(instructionTxt, signupUsername,resources.getString(R.string.signupUsernameTxt))
        onFocusSignup(instructionTxt, signupEmailPhoneNumber,resources.getString(R.string.signupEmailPhoneNumberTxt))
        onEntrySignup(noticeTxt,signupUsername)
        onEntrySignup(noticeTxt,signupEmailPhoneNumber)
    }

    fun switchEmailPhoneNumberFunc(textview: View) {
        signupEmailPhoneNumber.setText("")
        switchEmailPhoneNumber = !switchEmailPhoneNumber
        if (switchEmailPhoneNumber) {
            (textview as TextView).text = resources.getString(R.string.signupWithPhoneNumberLinkTxt)
            signupEmailPhoneNumber.hint = resources.getString(R.string.email)
            signupEmailPhoneNumber.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        }
        else {
            (textview as TextView).text = resources.getString(R.string.signupWithEmailLinkTxt)
            signupEmailPhoneNumber.hint = resources.getString(R.string.tel)
            signupEmailPhoneNumber.inputType = InputType.TYPE_CLASS_PHONE
        }
    }

    fun onNextPress(view: View) {
        //first check validity
        nextBtnUnique.isClickable = false
        var resStr: String = ""
        var count = 0
        val usernameTxt: String = signupUsername.text.toString().trim()
        if (!Validator.isValidName(usernameTxt)) {
            resStr += "username and "
            ++count
        }

        val emailPhoneNumberTxt: String = signupEmailPhoneNumber.text.toString().trim()
        if (switchEmailPhoneNumber && !Validator.isValidEmail(emailPhoneNumberTxt)) {
            resStr += "email and "
            ++count
        }
        else if (!switchEmailPhoneNumber && !Validator.isValidPhone(emailPhoneNumberTxt)) {
            resStr += "phone number and "
            ++count
        }
        if (resStr === "") {
            //valid, proceed to request
            requestUnique(usernameTxt, emailPhoneNumberTxt)
        }
        else {
            val len = resStr.length-5
            val _2b = if (count === 2) "are" else "is"
            resStr = resStr.substring(0,len)
            noticeTxt.text = "$resStr $_2b invalid or empty"
            nextBtnUnique.isClickable = true
        }
    }

    fun requestUnique(usernameTxt: String, emailPhoneNumberTxt: String) {
        //email
        val url = "http://${serverinfo["host"]}:${serverinfo["port"]}/signup/unique"
        val emailPhoneNumberStr: String = if (switchEmailPhoneNumber) "email" else "phoneNumber"
        val jsonStr = "{\"username\":\"$usernameTxt\",\"$emailPhoneNumberStr\":\"$emailPhoneNumberTxt\"}"
        val jsonObj = JSONObject(jsonStr)

        val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, url, jsonObj,
            { response ->
                val msg: String = response["message"].toString()
                val code = response["code"]

                if (code.equals(1000))
                {
                    //to the next activity
                    val name: String = intent.getStringExtra("name")
                    val intent = Intent(this, SignupPasswordActivity::class.java)
                    intent.putExtra("name",name)
                    intent.putExtra("username",usernameTxt)
                    intent.putExtra("emailPhoneNumberTxt",emailPhoneNumberTxt)
                    intent.putExtra("emailPhoneNumberStr",emailPhoneNumberStr)
                    startActivity(intent)
                }
                else {
                    noticeTxt.text = "$msg taken."
                    nextBtnUnique.isClickable = true
                }
                Log.i("uniqueRequest",response.toString())
            },
            { error ->
                Log.i("uniqueRequest",error.toString())
            }
        )

        AppVolleyRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }
}