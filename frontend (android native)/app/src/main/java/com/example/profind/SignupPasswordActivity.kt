package com.example.profind

import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_signup_password.*

class SignupPasswordActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup_password)

        onFocusSignup(instructionTxt, signupPasswd, resources.getString(R.string.signupPasswordTxt))
        onFocusSignup(instructionTxt, signupConfirmPasswd, resources.getString(R.string.signupConfirmPasswordTxt))
        onEntrySignup(noticeTxt,signupPasswd)
        onEntrySignup(noticeTxt,signupConfirmPasswd)
    }

    fun onNextPress(view: View) {
        nextBtnPasswd.isClickable = false
        val passwdTxt: String = signupPasswd.text.toString().trim()
        val confirmPasswdTxt: String = signupConfirmPasswd.text.toString().trim()

        Log.i("passwds",passwdTxt)
        Log.i("passwds",confirmPasswdTxt)

        if (!Validator.isValidPassword(passwdTxt)) {
            noticeTxt.text = resources.getString(R.string.passwdInvalidTxt)
            nextBtnPasswd.isClickable = true
        }
        else if (passwdTxt != confirmPasswdTxt) {
            noticeTxt.text = resources.getString(R.string.passwdDontMatchTxt)
            nextBtnPasswd.isClickable = true
        }
        else {
            //to the next activity
            val name: String? = intent.getStringExtra("name")
            val username: String? = intent.getStringExtra("username")
            val emailPhoneNumberTxt: String? = intent.getStringExtra("emailPhoneNumberTxt")
            val emailPhoneNumberStr: String? = intent.getStringExtra("emailPhoneNumberStr")
            val password = passwdTxt
        }
    }
}