package com.example.profind

import android.content.Intent
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_signup_name.*

class SignupNameActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup_name)

        onFocusSignup(instructionTxt, signupName,resources.getString(R.string.signupNameTxt))
        onEntrySignup(noticeTxt,signupName)
    }

    fun onNextPress(view: View) {
        nextBtnName.isClickable = false
        val nameTxt: String = signupName.text.toString().trim()
        if (!Validator.isValidName(nameTxt)) {
            noticeTxt.text = resources.getString(R.string.nameInvalidTxt)
            nextBtnName.isClickable = true
        }
        else {
            //to the next activity
            val intent = Intent(this, SignupUniqueActivity::class.java)
            intent.putExtra("name",nameTxt)
            startActivity(intent)
        }
    }
}