package com.example.profind

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream

open class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
    }
    val serverinfo : JSONObject by lazy {
        read_jsonObj("serverinfo.json")
    }
    fun read_jsonObj(filename: String): JSONObject {
        lateinit var jsonObj: JSONObject
        try {
            val inputStream : InputStream = assets.open(filename)
            val json = inputStream.bufferedReader().use { it.readText() }
            jsonObj = JSONObject(json)

        } catch (e: IOException){
            e.printStackTrace()
        }
        return jsonObj
    }

    //event handlers
    fun onFocusSignup(txtview: TextView, edittext: EditText, txt: String) {
        edittext.setOnFocusChangeListener { v, hasFocus ->
            txtview.text = if (hasFocus) txt else ""
        }
    }

    fun onEntrySignup(txtview: TextView, edittext: EditText) {
        edittext.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {}
            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) { txtview.text = "" }
        })
    }
}