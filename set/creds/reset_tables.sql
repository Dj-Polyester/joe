drop table if exists creds.options;
drop table if exists creds.users;
drop table if exists creds.codes;

/*! START TRANSACTION */;
CREATE TABLE IF NOT EXISTS creds.codes (
  emailphone VARCHAR(50) NOT NULL,
  code CHAR(128) NOT NULL UNIQUE,
  approved BOOLEAN NOT NULL,
  salt CHAR(20) NOT NULL UNIQUE,
  PRIMARY KEY(emailphone)
);
/*! COMMIT */

/*! START TRANSACTION */;
CREATE TABLE IF NOT EXISTS creds.users (
  userid UUID NOT NULL,
  name VARCHAR(50),
  username VARCHAR(50) NOT NULL UNIQUE,
  emailphone VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  salt CHAR(20) NOT NULL UNIQUE,
  PRIMARY KEY(userid),
  FOREIGN KEY (emailphone) REFERENCES creds.codes(emailphone)
);
/*! COMMIT */

/*! START TRANSACTION */;
CREATE TABLE IF NOT EXISTS creds.options (
  userid UUID NOT NULL,
  option VARCHAR(20) NOT NULL,
  value VARCHAR(20) NOT NULL,
  PRIMARY KEY(userid, option),
  FOREIGN KEY (userid) REFERENCES creds.users(userid)
);
/*! COMMIT */