from linkdbs import *
from resetdbs import *
from fillparentdb import *
from fillchilddb import *


if __name__ == "__main__":
    print("Resetting databases")
    resetdbs()
    print("Filling in the parent database")
    fillparentdb()
    print("Linking the databases")
    linkdbs()
    print("Filling in the child database")
    fillchilddb()