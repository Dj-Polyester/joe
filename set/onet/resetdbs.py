import psycopg2
from dbsql import *
from dbnames import *

def resetdb(conn, dbname):
    try:
        if conn is None:
            print('\tConnection not established to PostgreSQL.')
        else:
            print('\tConnection established to PostgreSQL.')

        cur = conn.cursor()

        if dbname == childdb:
            cur.execute("drop schema if exists public cascade")
            cur.execute("drop schema if exists onet cascade")
            cur.execute("drop schema if exists extensions cascade")
            cur.execute("create schema if not exists onet")
            cur.execute("create schema if not exists extensions")
        else:
            cur.execute("drop schema if exists public cascade")
            cur.execute("create schema if not exists public")
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        conn.close()
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('\tSchema reset succesfully for database {}, connection closed.'.format(dbname))

def resetdbs():
    resetparentdb()
    resetchilddb()

def resetchilddb():
    conn = psycopg2.connect( 
        dbname=childdb,
        user=username,
        password=password,
        host=host,
        port=port,
    )
    resetdb(conn, childdb)
def resetparentdb():
    conn = psycopg2.connect( 
        dbname=parentdb,
        user=username,
        password=password,
        host=host,
        port=port,
    )
    resetdb(conn, parentdb)

if __name__ == "__main__":
    resetdbs()     
