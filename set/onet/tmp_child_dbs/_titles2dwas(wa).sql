create table if not exists onet._titles2dwas as
with _titles2was_tmp as (
    select 
    onetsoc_code,
    element_id,
    data_value as factor
    from onet.work_activities
    where scale_id = 'IM'
),
_was2dwas_tmp as (
    select 
    element_id,
    dwa_id
    from onet.dwa_reference
)
select 
_titles2was_tmp.onetsoc_code,
_was2dwas_tmp.dwa_id,
_titles2was_tmp.factor
from _titles2was_tmp inner join _was2dwas_tmp on 
_titles2was_tmp.element_id = _was2dwas_tmp.element_id 
order by onetsoc_code, factor desc, dwa_id;
