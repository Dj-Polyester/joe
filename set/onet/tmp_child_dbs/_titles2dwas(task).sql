create table if not exists onet._titles2dwas as
with _titles2tasks_tmp as (
    select 
    onetsoc_code,
    task_id,
    data_value as factor
    from onet.task_ratings
    where scale_id = 'IM'
),
_tasks2dwas_tmp as (
    select 
    onetsoc_code,
    task_id,
    dwa_id
    from onet.tasks_to_dwas
)
select 
_tasks2dwas_tmp.onetsoc_code,
_tasks2dwas_tmp.dwa_id,
_titles2tasks_tmp.factor
from _titles2tasks_tmp inner join _tasks2dwas_tmp on 
_tasks2dwas_tmp.onetsoc_code = _titles2tasks_tmp.onetsoc_code and
_tasks2dwas_tmp.task_id = _titles2tasks_tmp.task_id
order by onetsoc_code, factor desc;
