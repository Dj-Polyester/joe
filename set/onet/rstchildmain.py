from fillchilddb import *
from resetdbs import resetchilddb
from linkdbs import *


if __name__ == "__main__":
    print("Resetting the child database")
    resetchilddb()
    print("Linking the databases")
    linkdbs()
    print("Filling in the child database")
    fillchilddb()