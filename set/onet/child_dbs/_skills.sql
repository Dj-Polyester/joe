create table if not exists onet._skills as
select 
element_id,
element_name,
description
from 
onet.content_model_reference
where (
    (element_id ILIKE '2.A%' OR element_id ILIKE '2.B%') 
    and length(element_id)=7
)
order by element_name;