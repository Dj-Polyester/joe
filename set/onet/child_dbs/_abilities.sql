create table if not exists onet._abilities as
select 
element_id,
element_name,
description
from 
onet.content_model_reference
where (
    element_id ilike '1.A%' and length(element_id)=9
)
order by element_name;