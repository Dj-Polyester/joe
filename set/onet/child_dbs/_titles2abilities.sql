create table if not exists onet._titles2abilities as
select 
onetsoc_code,
element_id,
data_value as factor
from onet.abilities
where scale_id = 'IM'
order by onetsoc_code, factor desc
