/*! START TRANSACTION */;
CREATE TABLE IF NOT EXISTS onet._job_zone_ref (
  job_zone DECIMAL(1,0) NOT NULL,
  name1 CHARACTER VARYING(50) NOT NULL,
  name2 CHARACTER VARYING(50),
  name3 CHARACTER VARYING(50),
  name4 CHARACTER VARYING(50),
  PRIMARY KEY (job_zone));
/*! COMMIT */;
/*! START TRANSACTION */;

INSERT INTO onet._job_zone_ref (job_zone, name1, name2, name3, name4) VALUES (1, 'No certificate needed',NULL,NULL,NULL);
INSERT INTO onet._job_zone_ref (job_zone, name1, name2, name3, name4) VALUES (2, 'High school diploma',NULL,NULL,NULL);
INSERT INTO onet._job_zone_ref (job_zone, name1, name2, name3, name4) VALUES (3, 'Vocational school diploma',NULL,NULL,NULL);
INSERT INTO onet._job_zone_ref (job_zone, name1, name2, name3, name4) VALUES (4, 'BS',NULL,NULL,NULL);
INSERT INTO onet._job_zone_ref (job_zone, name1, name2, name3, name4) VALUES (5, 'M.S.', 'Ph.D.', 'M.D.', 'J.D.');
/*! COMMIT */;