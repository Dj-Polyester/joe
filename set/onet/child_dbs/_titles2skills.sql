create table if not exists onet._titles2skills as
select 
onetsoc_code,
element_id,
data_value as factor
from onet.skills
where scale_id = 'IM'
order by onetsoc_code, factor desc
