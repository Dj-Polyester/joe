import psycopg2
from dbsql import *
from dbnames import *

def fillparentdb():
    try:
        conn = psycopg2.connect( 
            dbname=parentdb,
            user=username,
            password=password,
            host=host,
            port=port,
        )
    
        if conn is None:
            print('\tConnection not established to PostgreSQL.')
        else:
            print('\tConnection established to PostgreSQL.')

        cur = conn.cursor()

        with open("parent_dbs","r") as lines:
            for line in lines:
                if line[0] == "#":
                    continue
                refined_line = line.strip()
                with open("./db_mysql/{}.sql".format(refined_line),"r") as sqlfile:
                    print("\t{}".format(refined_line))
                    cur.execute(sqlfile.read())
            conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        conn.close()
        print(error)
    
    finally:
        if conn is not None:
            conn.close()
            print('\tThe parent database is succesfully filled, connection closed.')

if __name__ == "__main__":
    fillparentdb()
