### Set database tables ready for backend

```
python3 main.py
```

### Set child database table ready for backend

```
python3 rstchildmain.py
```

### NOTES

Before any operation, create a file `dbsql.py` with the credentials of the database with
the format

```
host =
port =
username =
user =
password =
```

The folder `child_dbs` contains files that query a table to be added to the child database.
The file `parent_dbs` hold the names of the tables to be added, chosen from `db_mysql` folder.
Other files with the extension `py` are also executable.
