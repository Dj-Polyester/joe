import psycopg2
from dbsql import *
from dbnames import *
import os

def fillchilddb():
    try:
        conn = psycopg2.connect( 
            dbname=childdb,
            user=username,
            password=password,
            host=host,
            port=port,
        )
    
        if conn is None:
            print('\tConnection not established to PostgreSQL.')
        else:
            print('\tConnection established to PostgreSQL.')

        cur = conn.cursor()

        parentdir = "./child_dbs"
        for filename in os.listdir(parentdir):
            print("\t{}".format(filename))
            with open("{}/{}".format(parentdir, filename),"r") as sqlfile:
                cur.execute(sqlfile.read())
            conn.commit()
    except (Exception, psycopg2.DatabaseError, FileNotFoundError) as error:
        conn.close()
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('\tThe child database is succesfully filled, connection closed.')

if __name__ == "__main__":
    fillchilddb()