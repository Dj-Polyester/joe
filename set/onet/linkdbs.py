import psycopg2
from dbsql import *
from dbnames import *


def linkdbs():
    try:
        conn = psycopg2.connect( 
            dbname=childdb,
            user=username,
            password=password,
            host=host,
            port=port,
        )

        if conn is None:
            print('\tConnection not established to PostgreSQL.')
        else:
            print('\tConnection established to PostgreSQL.')

        cur = conn.cursor()
        #small code, just download 
        cur.execute(
            "create extension if not exists \"uuid-ossp\" with schema extensions;"
        )
        cur.execute(
            "create extension if not exists postgres_fdw with schema extensions;"
        )
        cur.execute(
            "create server fdw_server foreign data wrapper postgres_fdw options (host '{}', dbname '{}', port '{}');"
            .format(host, parentdb, port)
        )
        cur.execute(
            "create user mapping for {} server fdw_server options (user '{}', password '{}');"
            .format(user , username, password)
        )
        cur.execute(
            "import foreign schema public from server fdw_server into onet;"
        )
        conn.commit()
        print('\tLinking succesful, connection closed.')
    except (Exception, psycopg2.DatabaseError) as error:
        conn.close()
        print(error)
    finally:
        if conn is not None:
            conn.close()

if __name__ == "__main__":
    linkdbs()